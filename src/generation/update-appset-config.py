#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import yaml
import os
import json
import subprocess
from colorama import init, Fore, Back, Style

class PipelineWriter:
  @staticmethod
  def generate_core_pipeline():
    generated_core_pipeline = """
---
include:
  - project: 'ixxel-pipelines/ci-templates'
    ref: update/june2022
    file:
    - jobs/git-clone.yaml
    - jobs/update-app-of-apps.yaml
    - jobs/git-simple-push.yaml
    - jobs/argocd-refresh.yaml

.docker_img_tests:
  image: ci-toolbox-ratp:latest

.docker_img_tools:
  image: cd-toolbox-ratp:latest

.pipeline_env:
  tags:
    - k8s-cluster
  retry:
    max: 2
    when:
    - unknown_failure
    - api_failure
    - stuck_or_timeout_failure
    - runner_system_failure

variables:
  ARGOCD_CLONE_DIR: gitops-argocd

stages:
- prepare
- update
- push-changes
- sync

clone-argocd-repo:
  stage: prepare
  extends:
  - .pipeline_env
  - .git-clone
  - .docker_img_tests
  variables:
    GIT_TOKEN: ${GITLAB_TOKEN}
    REPO_URL: https://gitlab.ratp.net/nubes/dso/gitops-repos/argocd.git
    REPO_BRANCH: main
    CLONE_DIR: ${ARGOCD_CLONE_DIR}

update-appset-conf:
  stage: update
  extends:
  - .pipeline_env
  - .update-app-of-apps
  - .docker_img_tests
  dependencies:
  - clone-argocd-repo
  parallel:
    matrix: []
  variables:
    WORKINGDIR: ${ARGOCD_CLONE_DIR}/argoApps
    SAVE_DIRECTORY: ${ARGOCD_CLONE_DIR}/**/*

git-push-appset-changes:
  stage: push-changes
  extends:
  - .pipeline_env
  - .git-simple-push
  dependencies:
  - clone-argocd-repo
  - update-appset-conf
  variables:
    GIT_TOKEN: ${GITLAB_TOKEN}
    REPO_URL: https://gitlab.ratp.net/nubes/dso/gitops-repos/argocd.git
    REPO_BRANCH: main
    GIT_WORKDIR: ${ARGOCD_CLONE_DIR}
    GIT_MSG: "CI/CD trigger from REPO ${CI_PROJECT_NAME}. User ${GITLAB_USER_NAME} branch ${CI_COMMIT_REF_NAME} commit ${CI_COMMIT_SHORT_SHA}"

argocd-refresh:
  stage: sync
  extends:
  - .pipeline_env
  - .argocd-refresh
  - .docker_img_tools
  variables:
    ARGOCD_SERVER: ${ARGOCD_SERVER}
    ARGOCD_API_TOKEN: ${ARGOCD_TOKEN}
    REFRESH: 'soft'
    SYNC: 'false'
    ARGOCD_APP_NAME: argocd-apps
"""
    return generated_core_pipeline

def load_json(json_file_path):
    try:
        json_file = open(json_file_path, 'r')
        try:
            python_dict = json.load(json_file)
        except json.decoder.JSONDecodeError:
            print(f"{Fore.RED}{Style.BRIGHT}[ ERROR ] >> File {json_file_path} could not be converted to json.")
            sys.exit(1)
    except IOError:
        print(f"{Fore.RED}{Style.BRIGHT}[ ERROR ] >> Failed to open file {json_file_path}.")
        sys.exit(1)
    return python_dict

def generator(app_list):
  with open('child-pipeline-gitlab-ci.yml', 'w+') as f:
    f.write(PipelineWriter.generate_core_pipeline())

  with open('child-pipeline-gitlab-ci.yml', 'r') as yamlRO:
    data = yaml.load(yamlRO, Loader=yaml.FullLoader)
    for app in app_list:
      data['update-appset-conf']['parallel']['matrix'].append({'CLUSTER_NAME': '${ENV}', 'CLUSTER_URL': '${APP_CLUSTER}', 'VALUE_FILE': 'values.yaml', 'CHART_PATH': '${ENV}', 'APPSET_YAML_FILE': (app_list[app])['APPSET_YAML_FILE']})

  with open('child-pipeline-gitlab-ci.yml', 'w') as yamlWR:
    yaml.dump(data, yamlWR)

if __name__ == "__main__":
  sys.stdout.isatty = sys.stderr.isatty = lambda: True
  init(autoreset=True)
  script_path = os.path.dirname(os.path.realpath(__file__))
  app_list = load_json(f'{script_path}/app-list2.json')
  generator(app_list)
