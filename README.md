# CI-CD Templates

## Helm push

- For HARBOR : https://<URL>/api/chartrepo/<REPONAME>/charts
- For NEXUS : https://<URL>/repository/<REPONAME>

## Trunk Based Development

Classic cherry-pick:
```bash
git cherry-pick -x 2a2eb6
```

Cherry-pick from merge commit:
```bash
git cherry-pick -x 2a2eb6 -m 1
```

When cherry-picking from trunk branch to release branch, pay attention to use the `--no-commit` flag to avoid accidental merge:
```bash
git cherry-pick --no-commit 2a2eb6
git add -A
git commit -m "cherry picked"
git push
```

Multiple cherry pick:
```bash
git cherry-pick commit1 commit2 commit3 commit4 commit5
```

Range cherry pick:
```bash
# A. INCLUDING the A commit
git cherry-pick A~..B
# OR (same as above)
git cherry-pick A~1..B
# OR (same as above)
git cherry-pick A^..B
# B. NOT including the A commit
git cherry-pick A..B
```

**A should be older than B, or A should be from another branch**

## git log

```bash
git log --all --decorate --oneline --graph
```

## Gitlab shallow clone parameters

Be careful for the git tag job to be working, you need to specify correct parameters in your git repository

<p align="center">
  <img src="src/pictures/shallow-parameters.png" width="55%" height="60%">
</p>

## Gitlab workflows

Typical application workflow rules using `Continuous Delivery` with `Trunk Based Development` based on `release branch` option and `promotion pipeline`:
```yaml
---
workflow:
  rules:
  - if: '$CI_COMMIT_TAG =~ /^v?[0-9]+\.[0-9]+\.[0-9]+$/'
    variables:
      TARGET_ENV: prod
    when: always
  - if: '$CI_COMMIT_TAG =~ /^v?[0-9]+\.[0-9]+\.[0-9]+-rc[0-9]+$/'
    variables:
      TARGET_ENV: pprod
    when: always
  - if: '$CI_COMMIT_REF_NAME =~ /^release\/[0-9]+\.[0-9]+$/'
    changes:
    - api/**/*
    variables:
      TARGET_ENV: staging
    when: always
  - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main"'
    changes:
    - api/**/*
    variables:
      TARGET_ENV: dev
    when: always
  - if: $CI_COMMIT_REF_NAME =~ /^feat\/.*/ || $CI_COMMIT_REF_NAME =~ /^fix\/.*/
    changes:
    - api/**/*
    variables:
      TARGET_ENV: temp
    when: always

stages:
  - ci
  - cd
  - promote

variables:
  BUILD:
    value: 'true'
    description: 'true to build app and chart, false to re-deploy only'

continuous-intregration:
  stage: ci
  variables:
    CODE_DIR: api
  rules:
  - if: $CI_COMMIT_TAG != null
    when: never
  - if: $BUILD == "true"
    when: on_success
  - when: never
  trigger:
    include:
      - project: toolchains/cicd-templates
        ref: ${TEMPLATES_REF}
        file: pipelines/ci-pipeline-env.yaml
    strategy: depend

continuous-deployment:
  stage: cd
  rules:
  - if: $CI_COMMIT_TAG != null
    when: never
  - if: '$CI_PIPELINE_SOURCE != "merge_request_event" && ($CI_COMMIT_REF_NAME =~ /^feat\/.*/ || $CI_COMMIT_REF_NAME =~ /^fix\/.*/)'
    when: never
  - when: on_success
  variables:
    ENV: ${TARGET_ENV}
    PROMOTED: 'false'
  trigger:
    include:
      - project: toolchains/cicd-templates
        ref: ${TEMPLATES_REF}
        file: pipelines/deployment-pipeline.yaml
    strategy: depend

promote-app-version:
  stage: promote
  rules:
  - if: $CI_COMMIT_TAG != null
    when: always
  - when: never
  variables:
    ENV: ${TARGET_ENV}
    PROMOTE_ARTEFACTS: ${BUILD}
  trigger:
    include:
      - project: toolchains/cicd-templates
        ref: ${TEMPLATES_REF}
        file: pipelines/promotion-pipeline.yaml
    strategy: depend
```

Typical application workflow rules using `Continuous Delivery` with classic `Trunk Based Development` branching model and `promotion pipeline`:
```yaml
workflow:
  rules:
  - if: '$CI_COMMIT_TAG =~ /^v?[0-9]+\.[0-9]+\.[0-9]+$/'
    variables:
      TARGET_ENV: prod
    when: always
  - if: '$CI_COMMIT_TAG =~ /^v?[0-9]+\.[0-9]+\.[0-9]+-rc[0-9]+$/'
    variables:
      TARGET_ENV: pprod
    when: always
  - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main"'
    changes:
    - api/**/*
    variables:
      TARGET_ENV: dev
    when: always
  - if: $CI_COMMIT_REF_NAME =~ /^feat\/.*/ || $CI_COMMIT_REF_NAME =~ /^fix\/.*/
    changes:
    - api/**/*
    variables:
      TARGET_ENV: temp
    when: always
```

## Gitlab groups/projects structure

Ideal structure:
```
structure/
└── root-group
    └── toolchains-group
        ├── code
        │   ├── app-A
        │   │   ├── microservice-1
        │   │   ├── microservice-2
        │   │   └── microservice-3
        │   └── app-B
        │       ├── microservice-1
        │       ├── microservice-2
        │       └── microservice-3
        └── manifests
            ├── app-A
            │   ├── microservice-1-helm
            │   ├── microservice-2-helm
            │   └── microservice-3-helm
            └── app-B
                ├── microservice-1-helm
                ├── microservice-2-helm
                └── microservice-3-helm
```

## Gitlab variables
### Root-group level variables
*Credentials*, *tokens*, *hostname* and high-level important variables should be at `root-group` level and only `administrators` should have `maintainers` permission on this group.

Mandatory variables are :

| Syntax      | Description | Value |
| ----------- | ----------- | ----------- |
| `RUNNER_TAG`      | the gitlab runner tag to be used       | *specific*
| `TEMPLATES_PROJECT`   | the gitlab server project path to look for cicd-templates        | *specific*
| `TEMPLATES_REF`   | branch used for the cicd-templates project        | *specific* default: `main`

example:

<p align="center">
  <img src="src/pictures/root-group-variables.png" width="55%" height="60%">
</p>

### Toolchains-group level variables
Important variables related to pipelines should be set at Toolchains-group level to make every application benefits from this configuration.

Mandatory variables are:

| Syntax      | Description | Value |
| ----------- | ----------- | ----------- |
| `ARGOCD_APP_NAMESPACE`| Kubernetes namespace where ArgoCD will deploy app       | `${APP_CODE}-${ENV_NAME}`
| `ARGOCD_APP_PROJECT`| ArgoCD project for the app       | `${APP_CODE}`
| `ARGOCD_DEPLOYMENT_MODE`| ArgoCD deployment type       | default: `helm`
| `ARGOCD_SERVER`| ArgoCD server url     | *specific*
| `CD_MANIFESTS`| Gitlab group for manifests      | example: `fabryk-apps/manifests/${APP_CODE}`
| `CD_MANIFESTS_REPO`| Gitlab projects pattern for manifests     | `${CI_SERVER_URL}/${CI_PROJECT_ROOT_NAMESPACE}/${CD_MANIFESTS}/${CI_PROJECT_NAME}-helm.git`
| `CHART_NAME`| name of helm chart      | `${FULL_NAME}`
| `IMAGE_NAME`| Image name      | `${FULL_NAME}`
| `FULL_NAME`| Full name of the microservice       | `${APP_CODE}-${MICROSERVICE_CODE}`
| `DOCKER_REG_RELEASE`| Release Docker registry | *specific*
| `DOCKER_REG_RELEASE_PASSWORD`| Release Docker registry password  | *specific*
| `DOCKER_REG_RELEASE_USER`| Release Docker registry user  | *specific*
| `DOCKER_REG_TEST`| Test Docker registry | *specific*
| `DOCKER_REG_TEST_PASSWORD`| Test Docker registry password| *specific*
| `DOCKER_REG_TEST_USER`| Test Docker registry user| *specific*
| `GITOPS_GIT_TOKEN`| Git token for GitOps repo     | *specific*
| `HELM_REG_RELEASE_PASSWORD`| Release Helm registry password      | *specific*
| `HELM_REG_RELEASE_TYPE`| Release Helm registry type      | `harbor`, `jfrog`, `nexus`
| `HELM_REG_RELEASE_URL`| Release Helm registry URL      | *specific*
| `HELM_REG_RELEASE_USER`| Release Helm registry user      | *specific*
| `HELM_REG_TEST_PASSWORD`| Test Helm registry password      | *specific*
| `HELM_REG_TEST_TYPE`| Test Helm registry type      | `harbor`, `jfrog`, `nexus`
| `HELM_REG_TEST_URL`| Test Helm registry URL      | *specific*
| `HELM_REG_TEST_USER`| Test Helm registry user      | *specific*
| `VAULT_APP_SECRET_PATH`| Path for the Vault secrets json      | `${APP_CODE}/${MICROSERVICE_CODE}/${ENV_NAME}/secrets`
| `VAULT_HELM_VALUES_PATH`| Path for the Vault secrets values       | `${APP_CODE}/${MICROSERVICE_CODE}/${ENV_NAME}/values`
| `VAULT_SERVER_URL`| Vault server URL    | *specific*


### Application group level variables

Application code name should be set at this level, as well as GitOps repository URL

| Syntax      | Description | Value |
| ----------- | ----------- | ----------- |
| `APP_CODE`| Name of the application      | *specific*
| `GITOPS_REPO_TARGET_BRANCH`| Branch of the GitOps repository     | *specific* default: `main`
| `GITOPS_REPO_URL`| GitOps repository URL  | *specific*
| `MANIFEST_REPO_REF`| Branch for the manifest repositories  | *specific*

<p align="center">
  <img src="src/pictures/app-group-variables.png" width="55%" height="60%">
</p>

### Microservice project level variables

At project level, you can define different environments for deployment allowing different set of variables. You need to specify environment aliases as well as microservice code name for each one of them.

| Syntax      | Description | Value |
| ----------- | ----------- | ----------- |
| `ENV_NAME`| Environment name. As many as environments.     | *specific*
| `MICROSERVICE_CODE`| Name of the microservice  | *specific*

<p align="center">
  <img src="src/pictures/project-variables.png" width="55%" height="60%">
</p>


Environments to define in the `Deployments` section
<p align="center">
  <img src="src/pictures/environments-gitlab.png" width="70%" height="60%">
</p>
