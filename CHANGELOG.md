# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

---
## [unreleased] - YYYY-MM-DD
### Changed
#### Jobs
- argocd-update-rbac.yaml multiple improvements
### Added
#### Pipelines
- argocd-app-of-apps.yaml pipeline will replace argocd-create-new-app.yaml pipeline
- terraform-init-aks.yaml
  - added applicationSet target duplicate avoidance mecanism
  - added applicationset target automatic removal before cluster destruction
  - cleared unecessary terraform apply duplicate in destroy workflow
#### Jobs
- argocd-generate-password.yaml
  - multiple improvements
- terraform-plan.yaml
  - added refresh-only mecanism
### Fixed
#### Jobs
- helm-template.yaml
  - fixed displaying issue
#### Pipelines
- manifest-pipeline.yaml
  - add update_chart_version: 'true' in chart-get-info
- update-chart.yaml
  - add update_chart_version: 'true' in chart-get-info
---
## [v2.3.0] - 2022-07-18
### Changed
#### Pipelines
- terraform-catalog-pipeline.yml : do not push terraform doc if pipeline triggered by a tag
#### Jobs
- variables names modified so the jobs can be called from gitlab include and gitlab trigger
- argocd-update-rbac multiple improvements
- argocd-generate-password multiple improvements
#### Scripts
- update-appset-list.py
  - changed variables case
  - changed app-list2 to app-list
### Added
- argocd-app-of-apps pipeline will replace argocd-create-new-app pipeline
- allow to configure checkov gate score (default value: 50)
  - app-pipeline.yaml
  - manifest-pipeline.yaml
  - terraform-artifactory-pipeline.yaml
  - terraform-catalog-pipeline.yaml
  - terraform-init-aks.yaml
  - terraform-pipeline.yaml
### Fixed
#### Pipelines
- using snippets for rules
  - terraform-artifactory-pipeline.yaml
  - terraform-catalog-pipeline.yaml
  - terraform-init-aks.yaml
  - terraform-pipeline.yaml
- removed extends image for all job in pipelines as it is already defined at the job level
- terraform-init-aks.yaml
  - changed variables case
#### Jobs
- values-get-info.yaml
  - lower to upper variables env
- argocd-refresh.yaml
  - changed variables case
#### Snippets
- ci_image upgraded from v1.4.1 to v1.4.2
