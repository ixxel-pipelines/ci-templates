---
include:
- project: ${TEMPLATES_PROJECT}
  ref: ${TEMPLATES_REF}
  file:
  - jobs/helm-template.yaml
  - jobs/git-clone.yaml
  - jobs/git-simple-push.yaml
  - jobs/split-helm-template.yaml
  - jobs/move-file.yaml
  - jobs/argocd-refresh.yaml
  - snippets/proxy.yaml
  - snippets/global-parameters.yaml

workflow:
  rules:
  - if: $CI_MERGE_REQUEST_IID
  - if: $CI_COMMIT_BRANCH
  - if: $CI_COMMIT_TAG

variables:
  ARGOCD_PLAINTEXT:
    description: ArgoCD disabling TLS
    value: 'true'
  ARGOCD_APPS_AOA: argocd-applications
  ARGOCD_CLONE_DIR: gitops-argocd
  APP_OF_APPS_REPO_URL: https://gitlab.com/ixxel-pipelines/app-of-apps.git
  APP_OF_APPS_REPO_REVISION: main
  APP_OF_APPS_CLONE_DIR: app-of-apps

.create_app_rule:
  rules:
  - if: $APP_CREATED == "false"

.env:
  environment: ${ENV}

stages:
- prepare
- templating
- apply-changes
- push
- sync

clone-app-of-apps-repo:
  stage: prepare
  extends:
  - .pipeline_env
  - .git-clone
  - .create_app_rule
  variables:
    GIT_TOKEN: ${GIT_APP_OF_APPS_TOKEN}
    REPO_URL: ${APP_OF_APPS_REPO_URL}
    REPO_BRANCH: ${APP_OF_APPS_REPO_REVISION}
    CLONE_DIR: ${APP_OF_APPS_CLONE_DIR}

clone-argocd-repo:
  stage: prepare
  extends:
  - .pipeline_env
  - .git-clone
  - .create_app_rule
  - .env
  variables:
    GIT_TOKEN: ${GIT_APP_OF_APPS_TOKEN}
    REPO_URL: ${GITOPS_ARGOCD_REPO_URL}
    REPO_BRANCH: ${GITOPS_ARGOCD_REPO_REVISION}
    CLONE_DIR: ${ARGOCD_CLONE_DIR}

helm-template:
  stage: templating
  extends:
  - .pipeline_env
  - .helm-template
  - .create_app_rule
  dependencies:
  - clone-app-of-apps-repo
  variables:
    WORKINGDIR: ${APP_OF_APPS_CLONE_DIR}/chart
    STDOUT_MODE: 'false'
    OUTPUT_DIRECTORY: helm-tpl
    SET: deployType=application|manifestType=helm|application.name=${APP_NAME}|application.namespace=${ARGOCD_APP_NAMESPACE}|application.destinationCluster=https://kubernetes.default.svc|application.project.name=${ARGOCD_APP_PROJECT}|application.repo.chart.path=${ENV}|application.repo.url=${APP_GITOPS_REPO_URL}|application.repo.revision=${APP_GITOPS_REPO_TARGET_BRANCH}

dispatch-files:
  stage: apply-changes
  allow_failure: true
  extends:
  - .pipeline_env
  - .move-file
  - .create_app_rule
  dependencies:
  - helm-template
  - clone-argocd-repo
  variables:
    WORKINGDIR: .
    SRC_FILE: helm-tpl/app-of-apps/templates/application*.yaml
    DST_DIR: ${ARGOCD_CLONE_DIR}/argoApps/app-${APP_NAME}.yaml
    SAVE_DIRECTORY: ${ARGOCD_CLONE_DIR}/**/*

commit-changes:
  stage: push
  allow_failure: false
  extends:
  - .pipeline_env
  - .git-simple-push
  - .create_app_rule
  - .env
  needs:
  - job: dispatch-files
  - job: clone-argocd-repo
    optional: true
  dependencies:
  - clone-argocd-repo
  - dispatch-files
  variables:
    GIT_TOKEN: ${GIT_APP_OF_APPS_TOKEN}
    REPO_URL: ${GITOPS_ARGOCD_REPO_URL}
    REPO_BRANCH: ${GITOPS_ARGOCD_REPO_REVISION}
    GIT_WORKDIR: ${ARGOCD_CLONE_DIR}
    GIT_MSG: Creating APP ${APP_NAME} in ArgoCD
    GIT_STAGING_DIR: .

argocd-refresh-app-of-apps:
  stage: sync
  extends:
  - .pipeline_env
  - .argocd-refresh
  - .create_app_rule
  - .env
  dependencies: []
  variables:
    ARGOCD_SERVER_HOST: ${ARGOCD_SERVER}
    ARGOCD_API_TOKEN: ${ARGOCD_TOKEN}
    REFRESH: soft
    SYNC: 'false'
    ARGOCD_APP_NAME: ${ARGOCD_APPS_AOA}
    PLAINTEXT: ${ARGOCD_PLAINTEXT}

argocd-refresh-app:
  stage: sync
  needs:
  - job: argocd-refresh-app-of-apps
    optional: true
  dependencies: []
  extends:
  - .pipeline_env
  - .env
  - .argocd-refresh
  variables:
    ARGOCD_SERVER_HOST: ${ARGOCD_SERVER}
    ARGOCD_API_TOKEN: ${ARGOCD_TOKEN}
    ARGOCD_APP_NAME: ${APP_NAME}
    REFRESH: soft
    SYNC: 'false'
    PLAINTEXT: ${ARGOCD_PLAINTEXT}
