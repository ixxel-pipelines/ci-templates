---
include:
- project: ${TEMPLATES_PROJECT}
  ref: ${TEMPLATES_REF}
  file:
  - snippets/images.yaml

.chart-get-info:
  extends:
  - .cd-image
  variables:
    INCREMENT: fix
    UNRELEASED: 'false'
    UPDATE_CHART: 'false'
    APP_VERSION: ''
    WORKINGDIR: .
  before_script:
  - |-
    RED="\033[1;31m"
    GREEN="\033[1;32m"
    YELLOW="\033[1;33m"
    BLUE="\033[1;34m"
    PURPLE="\033[1;35m"
    CYAN="\033[1;36m"
    BLANK="\033[0m"

    echo -e "${BLUE}[ STEP - Helm-chart info fetching ] > Fetching Helm chart info.${BLANK}"
    echo -e "${CYAN}[ INFO ] > Helm version.${BLANK}"
    helm version --short
  script:
  - |-
    # shellcheck disable=SC2206
    set +e
    if [[ -f ${WORKINGDIR}/Chart.yaml ]]
    then
      echo -e "${CYAN}[ INFO ] > Chart.yaml file detected. Here is the file${BLANK}"
      yq e ${WORKINGDIR}/Chart.yaml -C
      CHART_NAME=$(yq e '.name' ${WORKINGDIR}/Chart.yaml)
      CHART_VERSION=$(yq e '.version' ${WORKINGDIR}/Chart.yaml)
      CHART_APP_VERSION=$(yq e '.appVersion' ${WORKINGDIR}/Chart.yaml)
      echo -e "${CYAN}[ INFO ] > The chart [[ ${YELLOW}${CHART_NAME}${CYAN} ]] has version --> [[ ${YELLOW}${CHART_VERSION}${CYAN} ]] and appVersion --> [[ ${YELLOW}${CHART_APP_VERSION}${CYAN} ]]${BLANK}"
    else
      echo -e "${RED}[ ERROR ] > Chart.yaml file NOT detected.${BLANK}"
      exit 1
    fi

    REGEX="^([0-9]+\.[0-9]+\.[0-9]+)$"

    if [[ ${UPDATE_CHART} == "true" ]]
    then
      if [[ ${UNRELEASED} == "true" ]]
      then
        echo -e "${PURPLE}[ EVENT - INCREMENT + UNRELEASED ] > This Section has been triggered by the Application pipeline to auto update the manifest version and REMOVE unreleased tag${BLANK}"
        CHART_VERSION=$(echo "${CHART_VERSION}" | sed -E "s|-.+||" )
      else
        echo -e "${PURPLE}[ EVENT - INCREMENT ] > This Section has been triggered by the Application pipeline to auto update the manifest version${BLANK}"
      fi

      if [[ ${CHART_VERSION} =~ ${REGEX} ]]
      then
        chart_ver=( ${CHART_VERSION//./ } )
        if [[ ${INCREMENT} = "fix" ]]
        then
          echo -e "${CYAN}[ INFO ] > Fix increment${BLANK}"
          ((chart_ver[2]++))
        elif [[ ${INCREMENT} = "minor" ]]
        then
          echo -e "${CYAN}[ INFO ] > Minor increment${BLANK}"
          ((chart_ver[1]++))
          chart_ver[2]=0
        elif [[ ${INCREMENT} = "major" ]]
        then
          echo -e "${CYAN}[ INFO ] > Major increment${BLANK}"
          ((chart_ver[0]++))
          chart_ver[2]=0
          chart_ver[1]=0
        else
          echo -e "${RED}[ ERROR ] > increment ${INCREMENT} is not correct${BLANK}"
          exit 1
        fi
        if [[ -n "${APP_VERSION}" ]]
        then
          CHART_APP_VERSION=${APP_VERSION}
        fi
        CHART_NEW_VERSION=${chart_ver[0]}.${chart_ver[1]}.${chart_ver[2]}
        CHART_VERSION=${CHART_NEW_VERSION}
        echo -e "${CYAN}[ INFO ] > The new chart version will be --> [[ ${YELLOW}${CHART_NEW_VERSION}${CYAN} ]] and appVersion --> [[ ${YELLOW}${CHART_APP_VERSION}${CYAN} ]]${BLANK}"
      else
        echo -e "${RED}[ ERROR ] > Chart version doesn't match regex.${BLANK}"
        exit 1
      fi
    elif [[ ${UNRELEASED} == "true" ]]
    then
      echo -e "${PURPLE}[ EVENT - UNRELEASED ] > This Section has been triggered by the Application pipeline to ADD unreleased tag${BLANK}"
      if [[ ${CHART_VERSION} =~ ${REGEX} ]]
      then
        APP_VERSION_SHORT_SHA=$(echo "${APP_VERSION}" | sed 's/unreleased-//')
        echo -e "${CYAN}[ INFO ] > Short SHA of appVersion is ${PURPLE}${APP_VERSION_SHORT_SHA}${BLANK}"
        CHART_NEW_VERSION=${CHART_VERSION}-${APP_VERSION_SHORT_SHA}
        if [[ -n "${APP_VERSION}" ]]
        then
          CHART_APP_VERSION=${APP_VERSION}
        fi
        echo -e "${CYAN}[ INFO ] > The new chart version will be --> [[ ${YELLOW}${CHART_NEW_VERSION}${CYAN} ]] and appVersion --> [[ ${YELLOW}${CHART_APP_VERSION}${CYAN} ]]${BLANK}"
        CHART_VERSION=${CHART_NEW_VERSION}
        CHART_APP_VERSION=${APP_VERSION}
      else
        echo -e "${RED}[ ERROR ] > Chart version doesn't match regex.${BLANK}"
        exit 1
      fi
    fi
    echo "CHART_NAME=${CHART_NAME}" >> chart-get-info.env
    echo "CHART_VERSION=${CHART_VERSION}" >> chart-get-info.env
    echo "CHART_APP_VERSION=${CHART_APP_VERSION}" >> chart-get-info.env
    echo "FETCHED_CHART_VERSION=${CHART_VERSION}" >> chart-get-info.env
    echo "FETCHED_CHART_APP_VERSION=${CHART_APP_VERSION}" >> chart-get-info.env
  artifacts:
    reports:
      dotenv: chart-get-info.env
