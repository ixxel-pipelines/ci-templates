---
include:
- project: ${TEMPLATES_PROJECT}
  ref: ${TEMPLATES_REF}
  file:
  - snippets/images.yaml

.trivy-scan:
  extends:
  - .cd-image
  allow_failure: true
  variables:
    IMG_REFERENCE: ''                                     # image reference(for backward compatibility)
    SCAN_REF: .
    SCAN_TYPE: image                                      # Scan type to use for scanning vulnerability (image, filesystem, repository, config)
    INPUT: ''                                             # input file path instead of image name
    TEMPLATE: ''                                          # use an existing template for rendering output (@/contrib/sarif.tpl, @/contrib/gitlab.tpl, @/contrib/junit.tpl
    OUTPUT: ''                                            # output file name
    SKIP_DIRS: ''                                         # specify the directories where the traversal is skipped
    EXIT_CODE: '1'                                        # Exit code when vulnerabilities were found
    VULN_TYPE: os,library                                 # comma-separated list of vulnerability types (os,library)
    SECURITY_CHECKS: vuln                                 # comma-separated list of what security issues to detect (vuln,config)
    SEVERITY: UNKNOWN,LOW,MEDIUM,HIGH,CRITICAL            # severities of vulnerabilities to be displayed (comma separated)
    FORMAT: table                                         # format (table, json, template)
    WORKINGDIR: .
  before_script:
  - |-
    RED="\033[1;31m"
    GREEN="\033[1;32m"
    YELLOW="\033[1;33m"
    BLUE="\033[1;34m"
    PURPLE="\033[1;35m"
    CYAN="\033[1;36m"
    BLANK="\033[0m"

    echo -e "${BLUE}[ STEP - VULNERABILITIES CHECK ] > Scanning ${IMG_REFERENCE} for vulnerabilities.${BLANK}"
    echo -e "${CYAN}[ INFO ] > Trivy version.${BLANK}"
    trivy --version | head -n 1
    echo -e "${YELLOW}[ EXECUTING ] > Updating Trivy DB.${BLANK}"
    trivy image --download-db-only --no-progress
    echo ""

    export ARTIFACT_REF=${IMG_REFERENCE}
    if [ "${SCAN_TYPE}" = "fs" ] ||  [ "${SCAN_TYPE}" = "config" ];then
      echo -e "${CYAN}[ INFO ] > Trivy scan type fs/config.${BLANK}"
      ARTIFACT_REF=${SCAN_REF}
    fi

    if [ "${INPUT}" ]; then
      echo -e "${CYAN}[ INFO ] > Trivy scanning archive ${INPUT}.${BLANK}"
      if [ "${ARCHIVE_TYPE}" = "oci-archive" ]
      then
        echo -e "${CYAN}[ INFO ] > oci type archive, using skopeo to get oci dir type${BLANK}"
        skopeo copy oci-archive:${INPUT} oci:/tmp/extracted-img
        export INPUT=/tmp/extracted-img
      fi
      ARTIFACT_REF="--input ${INPUT}"
    fi

    ARGS=""
    if [ "${TEMPLATE}" ]; then
      echo -e "${CYAN}[ INFO ] > Trivy scan using template ${TEMPLATE}.${BLANK}"
      ARGS="$ARGS --template ${TEMPLATE}"
    fi

    if [ "${OUTPUT}" ];then
      ARGS="$ARGS --output ${OUTPUT}"
    fi

    if [ "${SKIP_DIRS}" ];then
      str=${SKIP_DIRS}
      final=${str//,/ --skip-dirs }
      ARGS="$ARGS --skip-dirs ${final}"
    fi
  script:
  - |-
    set +e
    echo -e "${YELLOW}[ EXECUTING ] > Executing ${IMG_REFERENCE} vuln scan.${BLANK}"
    echo -e "${CYAN}[ INFO ] > Scan mode : ${SCAN_TYPE}, on artifact(s) : ${ARTIFACT_REF}.${BLANK}"

    if [ ! ${SCAN_TYPE} = "config" ]
    then
      script -q -e -c "trivy ${SCAN_TYPE} --exit-code ${EXIT_CODE} \
                                          --vuln-type ${VULN_TYPE} \
                                          --security-checks ${SECURITY_CHECKS} \
                                          --severity ${SEVERITY} \
                                          --format ${FORMAT} \
                                          ${ARGS} \
                                          ${ARTIFACT_REF}"
    else
      script -q -e -c "trivy ${SCAN_TYPE} --exit-code ${EXIT_CODE} \
                                          --severity ${SEVERITY} \
                                          --format ${FORMAT} \
                                          ${ARGS} \
                                          ${ARTIFACT_REF}"
    fi
    EXIT_CODE="${PIPESTATUS[0]}"

    if [[ ${EXIT_CODE} -ne 0 ]]
    then
      echo -e "${RED}[ FAIL ] > Trivy scan failed !${BLANK}"
      exit ${EXIT_CODE}
    else
      echo -e "${GREEN}[ SUCCESS ] > Trivy scan succeeded !${BLANK}"
    fi
  artifacts:
    when: always
    paths:
    - ${OUTPUT}
    expire_in: 1 day
