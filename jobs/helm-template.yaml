---
include:
- project: ${TEMPLATES_PROJECT}
  ref: ${TEMPLATES_REF}
  file:
  - snippets/images.yaml

.helm-template:
  extends:
  - .cd-image
  variables:
    STDOUT_MODE: 'true'
    OUTPUT_DIRECTORY: helm-tpl-result
    WORKINGDIR: .
    CHART_NAME: ''
    CHART_VERSION: ''
    CHART_APP_VERSION: ''
    OUTPUT_FILE: result.yaml
    DISPLAY: 'true'
    VALUES_FILE: ${WORKINGDIR}/values.yaml
    SET: ''
    ADD_REPO: 'false'
    HELM_REPO_NAME: default
    HELM_REPO_URL: ''
    HELM_REPO_USER: ''
    HELM_REPO_PWD: ''
  before_script:
  - |-
    RED="\033[1;31m"
    GREEN="\033[1;32m"
    YELLOW="\033[1;33m"
    BLUE="\033[1;34m"
    PURPLE="\033[1;35m"
    CYAN="\033[1;36m"
    BLANK="\033[0m"
    set +e

    echo -e "${BLUE}[ STEP - Helm-chart templating ] > Templating Helm chart.${BLANK}"
    echo -e "${CYAN}[ INFO ] > Helm version.${BLANK}"
    helm version --short

    if [[ ${ADD_REPO} = "true" ]]
    then
      echo -e "${YELLOW}[ EXECUTING ] > Adding repo named ${HELM_REPO_NAME} from ${HELM_REPO_URL}.${BLANK}"
      helm repo add ${HELM_REPO_NAME} ${HELM_REPO_URL} --force-update \
                                                      --pass-credentials \
                                                      --username=${HELM_REPO_USER} \
                                                      --password=${HELM_REPO_PWD} \
                                                      --insecure-skip-tls-verify

      if [[ $? = 0 ]]
      then
        echo -e "${GREEN}[ SUCCESS ] > Helm repo added successfully !${BLANK}"
        WORKINGDIR=${HELM_REPO_NAME}/${CHART_NAME}
      else
        echo -e "${RED}[ ERROR ] > Helm repo failed to be added.${BLANK}"
        exit 1
      fi
    fi

    if [ "${SET}" ]
    then
      [[ ! "$SET" =~ ^\|.+ ]] || SET="${SET:1}"
      full_str=${SET//|/ --set }
      SETTINGS="$SETTINGS --set $full_str"
      echo -e "${CYAN}[ INFO ] > Set values for helm template are the following${BLANK}"
      echo $SETTINGS
    fi
  script:
  - |-
    if [[ ${ADD_REPO} = "false" ]]
    then
      if [[ -f ${WORKINGDIR}/Chart.yaml ]]
      then
        echo -e "${CYAN}[ INFO ] > Chart.yaml file detected.${BLANK}"
        echo -e "${CYAN}[ INFO ] > The chart [[ ${YELLOW}${CHART_NAME}${CYAN} ]] has version --> [[ ${YELLOW}${CHART_VERSION}${CYAN} ]] and appVersion --> [[ ${YELLOW}${CHART_APP_VERSION}${CYAN} ]]${BLANK}"
      else
        echo -e "${PURPLE}[ WARNING ] > Chart.yaml file NOT detected.${BLANK}"
      fi
    fi

    echo -e "${YELLOW}[ EXECUTING ] > Executing helm chart ${CHART_NAME} templating.${BLANK}"
    if [[ ! -d ${OUTPUT_DIRECTORY} ]]; then mkdir -p ${OUTPUT_DIRECTORY}; fi
    if [[ ${STDOUT_MODE} = "true" ]]
    then
      helm template ${CHART_NAME} ${WORKINGDIR} --values="${VALUES_FILE}" ${SETTINGS} > ${OUTPUT_DIRECTORY}/${OUTPUT_FILE}
    elif [[ ${STDOUT_MODE} = "false" ]]
    then
      helm template ${CHART_NAME} ${WORKINGDIR} --values="${VALUES_FILE}" ${SETTINGS} --output-dir ${OUTPUT_DIRECTORY}
    fi

    if [[ $? = 0 ]]
    then
      if [[ ${STDOUT_MODE} = "true" ]]
      then
        TPL_CHART_YAML=${OUTPUT_DIRECTORY}/${OUTPUT_FILE}
        echo "TPL_CHART_YAML=${TPL_CHART_YAML}" >> helm-template.env
        if [[ "${DISPLAY}" = "true" ]]
        then
          echo -e "${CYAN}[ INFO ] > Displaying templated result.${BLANK}"
          yq e -C ${OUTPUT_DIRECTORY}/${OUTPUT_FILE}
        fi
      fi
      echo -e "${GREEN}[ CHECK SUCCESS ] > Helm templating succeeded without any error.${BLANK}"
    else
      echo -e "${RED}[ CHECK ERROR ] > Helm templating didn't succeed !${BLANK}"
      exit 1
    fi
  artifacts:
    reports:
      dotenv: helm-template.env
    paths:
    - ${OUTPUT_DIRECTORY}/**/*
    expire_in: 10 mins
